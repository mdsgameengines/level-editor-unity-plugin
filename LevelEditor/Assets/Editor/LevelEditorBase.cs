﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


public class LevelEditorBase : SceneView {

    public static LevelEditorBase Instance { get; private set; }

    public static GameObject level;
    static GameObject sceneCamObject;
    public static LevelEditorBase window;

    //public Dictionary<int, GameObject> gridDict = new Dictionary<int, GameObject>();
    //public Dictionary<int, int> gridHeightDict = new Dictionary<int, int>();
    //public GridScript gridClass;
    public Vector2Int gridSize;
    public Vector2 nodeSize;
    public float gridSnapValue;
    static LayerMask mask;

    public float cameraOrbit = 45f;
    public float cameraPitch = 0f;
    public float cameraZoom = 15f;

    static Texture2D tex;

    //static SceneViewRotation rot;
    //Toolbar buttons
    LEToolbar toolbar;
    /*bool selectButton;
    bool rotateButton;
    bool deleteButton;
    bool paintButton;*/
    int toolbarInt = 0;
    public string[] toolbarStrings = new string[] { "Toolbar1", "Toolbar2", "Toolbar3" };
    List<GameObject> objectsSelected;
    GameObject objectSelected;
    PreviewWindow preview;
    static public LevelScript levelScript;

    //Preview Window
    //PreviewWindow preview;

    [MenuItem("Level Editor/Create New Level")]
    static void CreateWindow()
    {
        mask = LayerMask.NameToLayer("LevelEditor");
        if (mask == -1)
        {
            throw new UnassignedReferenceException("Layer LevelEditor must be created in the Layer Manager");
        }
        window = (LevelEditorBase)GetWindow(typeof(LevelEditorBase));
        level = new GameObject
        {
            name = "Level"
        };
        levelScript = level.AddComponent<LevelScript>();
        levelScript.previewGrid = new GameObject().transform;
        levelScript.previewGrid.parent = level.transform;
        level.GetComponent<LevelScript>().window = window;
        levelScript.mask = mask;
        levelScript.gridClass = levelScript.gameObject.AddComponent<GridScript>();
        levelScript.gridClass.mask = mask.value;
        levelScript.gridClass.level = level;

        window.name = "Level Editor";
        window.titleContent = new GUIContent("Level Editor");
        Tools.lockedLayers = (1 << mask.value);

    }


    public override void OnEnable()
    {
        base.OnEnable();
        Debug.Log("Was Enabled");

        Instance = this;
        ToggleGizmosScript.ToggleGizmos(false);
        window = (LevelEditorBase)GetWindow(typeof(LevelEditorBase));
        window.titleContent = new GUIContent("Level Editor");
        ToggleGridUtility.ShowGrid = false;

        toolbar = new LEToolbar();
        toolbar.toolSelected = ToolSelected.None;

        preview = new PreviewWindow();
        //toolbar.SetupToolbar();

        if (SceneView.onSceneGUIDelegate != null)
        {
            // Add (or re-add) the delegate.
            SceneView.onSceneGUIDelegate += this.OnSceneGUI;
        }

        
    }

    public override void OnDisable()
    {
        base.OnDisable();
        ToggleGizmosScript.ToggleGizmos(true);
        ToggleGridUtility.ShowGrid = true;
    }

    public void SettingsWindow(int id)
    {

        GUILayout.Label("Grid Settings");
        GUILayout.BeginVertical();
        GUILayout.BeginHorizontal();
        ToggleGridUtility.ShowGrid = false;

        Vector2Int prevGridSize = gridSize;
        gridSize = EditorGUILayout.Vector2IntField("Grid Size", gridSize);
        levelScript.gridClass.gridSize = gridSize;
        GUILayout.EndHorizontal();

        Vector2 prevSize = nodeSize;
        nodeSize = EditorGUILayout.Vector2Field("Node Size", nodeSize);
        levelScript.gridClass.nodeSize = nodeSize;

        if(prevSize != nodeSize || prevGridSize != gridSize)
        {
            if(levelScript.previewGrid.childCount != 0)
            {
                DestroyImmediate(levelScript.previewGrid.transform.GetChild(0).gameObject);
            }

            levelScript.gridClass.CreatePreviewGrid(levelScript.previewGrid);
        }


        Layers();
        
        bool buttonGridPressed = GUILayout.Button("Draw Grid");
        if (buttonGridPressed)
        {
            //adicionar check a ver se ja ha uma grid nesta layer
            if (!levelScript.gridClass.gridDict.ContainsKey(levelScript.currentLayer))
            {
                if (!levelScript.gridClass.gridHeightDict.ContainsValue(levelScript.layerHeight))
                {
                    //levelScript.gridClass.level = level;
                    //levelScript.gridClass.mask = mask;
                    levelScript.gridClass.CreateGrid(levelScript.currentLayer, levelScript.layerHeight, false);
                }
            }

        }
        GUILayout.Label("Object Preview");

        GUILayout.BeginHorizontal();
        GUILayout.Label("Camera Orbit");
        cameraOrbit = EditorGUILayout.Slider(cameraOrbit, 0f, 360f);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Camera Pitch");
        cameraPitch = EditorGUILayout.Slider(cameraPitch, -90, 90);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Camera Zoom");
        cameraZoom = EditorGUILayout.Slider(cameraZoom, 1, 110);
        GUILayout.EndHorizontal();


        preview.cameraOrbit = cameraOrbit;
        preview.cameraPitch = cameraPitch;
        preview.cameraZoom = cameraZoom;

        bool camDefault = GUILayout.Button("Reset to default");
        if (camDefault)
        {
            cameraOrbit = 45f;
            cameraPitch = 0f;
            cameraZoom = 15f;
        }

        EditorGUI.DrawPreviewTexture(new Rect(0, 300, 269, 149), preview.DrawRenderPreview(new Rect(0, 0, 269, 149)));

        GUILayout.EndVertical();
    }

    public void LevelEditorToolbar(int id)
    {
        GUILayout.Label("Level Editor Toolbar");
        //GUILayout.BeginVertical();
        GUILayout.BeginHorizontal();
        
        toolbar.SetupToolbar();
        toolbar.CheckToggleState();
        GUILayout.EndHorizontal();
    }
    
    void Layers()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Current Layer");
        GUILayout.EndHorizontal();
           

        GUILayout.BeginHorizontal();
        int prevLayer = levelScript.currentLayer;
        levelScript.currentLayer = EditorGUILayout.IntSlider(levelScript.currentLayer, 0, levelScript.layerInt);
        if(prevLayer != levelScript.currentLayer)
        {
            levelScript.gridClass.ActivateGrid(levelScript.currentLayer);
        }
        bool plusLayer = GUILayout.Button("+");
        GUILayout.EndHorizontal();

        if (plusLayer)
        {
            levelScript.layerInt++;
            levelScript.currentLayer = levelScript.layerInt;
        }

        GUILayout.BeginHorizontal();
        float prevHeight = levelScript.layerHeight;
        levelScript.layerHeight = EditorGUILayout.FloatField("Height", levelScript.layerHeight);
        if(prevHeight != levelScript.layerHeight)
        {
            if(levelScript.previewGrid.childCount == 0)
            {
                levelScript.gridClass.CreatePreviewGrid(levelScript.previewGrid);
            }

            MoveGrid(levelScript.previewGrid.GetChild(0), levelScript.layerHeight-prevHeight);
        }
        GUILayout.EndHorizontal();

        gridSnapValue = EditorGUILayout.FloatField("Grid Snap Value", gridSnapValue);
    }

   public void OnSceneGUI(SceneView sceneView)
   {
        
        if (currentDrawingSceneView.titleContent.text == "Level Editor")
        {
            currentDrawingSceneView.sceneViewState.showSkybox = false;
            //Camera.current.clearFlags = CameraClearFlags.SolidColor;
            //Camera.current.backgroundColor = Color.black;
            
            GUILayout.Window(0, new Rect(0, 20, 270, 450), SettingsWindow, "Settings");
            GUILayout.Window(1, new Rect(300, 20, 200, 70), LevelEditorToolbar, "Toolbar");
            //GUILayout.Window(2, new Rect(600, 20, 200, 70), )
            //toolbarInt = GUI.Toolbar(new Rect(300, 20, 200, 70), toolbarInt, toolbarStrings);
            ControlHandling();
            //toolbar.SetupToolbar();
        }
   }

    public void ControlHandling()
    {
        Event e = Event.current;
        int controlID = GUIUtility.GetControlID(FocusType.Passive);
        switch (e.GetTypeForControl(controlID))
        {
            case EventType.MouseDown:
                if (e.button == 0 && toolbar.toolSelected != ToolSelected.Select && toolbar.toolSelected != ToolSelected.None)
                {
                    GUIUtility.hotControl = controlID;
                    e.Use();
                }

                
                break;
            case EventType.MouseUp:
                GUIUtility.hotControl = 0;
                if (e.button == 0 && toolbar.toolSelected == ToolSelected.Insert)
                {
                    Ray worldRay = HandleUtility.GUIPointToWorldRay(e.mousePosition);
                    RaycastHit hitInfo;
                    // Shoot this ray. check in a distance of 10000.
                    if (Physics.Raycast(worldRay, out hitInfo, Mathf.Infinity))
                    {
                        // Instance this prefab
                        GameObject prefab_instance = PrefabUtility.InstantiatePrefab(Selection.activeGameObject) as GameObject;
                        // Place the prefab at correct position (position of the hit).
                        prefab_instance.transform.position = hitInfo.transform.position;
                        prefab_instance.transform.SetParent(level.transform);
                        // Mark the instance as dirty because we like dirty
                        EditorUtility.SetDirty(prefab_instance);
                        Debug.Log(hitInfo.transform.name);
                    }
                    e.Use();
                }
                e.Use();
                break;
            case EventType.MouseDrag:

                /*if (e.button == 0)
                {
                    GUIUtility.hotControl = controlID;
                    e.Use();
                }*/

                
                break;
            case EventType.ScrollWheel:
                if (e.shift)
                {
                    //MoveGrid(level.transform.GetChild(0), e.delta.y);
                    float valueToIncrease = 0;
                    if (gridSnapValue <= 0)
                    {
                        valueToIncrease = e.delta.y;
                        //levelScript.layerHeight += e.delta.y;
                    } else
                    {
                        valueToIncrease = gridSnapValue * (e.delta.y / Mathf.Abs(e.delta.y));
                        Debug.Log("GotHere");
                    }
                    levelScript.layerHeight += valueToIncrease;
                    if (levelScript.previewGrid.GetChild(0)) {
                        MoveGrid(levelScript.previewGrid.GetChild(0), valueToIncrease);
                    }
                    Debug.Log(e.delta);
                    e.Use();
                }
                break;
            case EventType.KeyDown:
                if (e.keyCode == KeyCode.Escape)
                {
                    // Do something on pressing Escape
                }
                if (e.keyCode == KeyCode.Space)
                {
                    // Do something on pressing Spcae
                }
                if (e.keyCode == KeyCode.S)
                {
                    // Do something on pressing S
                }
                break;
        }
    }

    public static void ShowWindow()
    {
        window = GetWindow<LevelEditorBase>();
        window.titleContent = new GUIContent("Level Editor");
    }

    public static void CloseWindow()
    {
        window.Close();
    }

    public void MoveGrid(Transform t, float y)
    {
        Vector3 pos = t.position;
        pos.y += y;
        levelScript.previewGrid.position = pos;
        foreach(LineRenderer l in t.GetComponentsInChildren<LineRenderer>())
        {
            Vector3 pos1 = l.GetPosition(0);
            Vector3 pos2 = l.GetPosition(1);
            pos1.y = 0.01f + pos.y;
            pos2.y = 0.01f + pos.y;

            l.SetPosition(0, pos1);
            l.SetPosition(1, pos2);

        }
    }
}


