﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class LEToolbar {

    //Toolbar buttons
    bool insertButton;
    bool selectButton;
    bool rotateButton;
    bool deleteButton;
    bool paintButton;

    List<GameObject> objectsSelected;
    GameObject objectSelected;

    public ToolSelected toolSelected;
    
    public void SetupToolbar()
    {
        insertButton = GUILayout.Toggle(insertButton, "Insert", GUILayout.Width(60));
        selectButton = GUILayout.Toggle(selectButton, "Select", GUILayout.Width(60));
        rotateButton = GUILayout.Toggle(rotateButton, "Rotate", GUILayout.Width(60));   
        //deleteButton = GUILayout.Toggle(deleteButton, "D", GUILayout.Width(25));
        deleteButton = GUILayout.Button("Delete", GUILayout.Width(50));
        paintButton = GUILayout.Toggle(paintButton, "Paint", GUILayout.Width(50));
    }
    public void Debugs()
    {
        Debug.Log(insertButton);
        Debug.Log(selectButton);
        Debug.Log(rotateButton);
        Debug.Log(deleteButton);
        Debug.Log(paintButton);
    }

    public void CheckToggleState()
    {
        if (insertButton && toolSelected != ToolSelected.Insert)
        {
            ChangeToolSelected(ToolSelected.Insert);
        }
        if (selectButton && toolSelected != ToolSelected.Select)
        {
            ChangeToolSelected(ToolSelected.Select);
        }
        if (rotateButton && toolSelected != ToolSelected.Rotate)
        {
            ChangeToolSelected(ToolSelected.Rotate);
        }
        if (deleteButton && toolSelected != ToolSelected.Delete)
        {
            DeleteObjects();
        }
        if (paintButton && toolSelected != ToolSelected.Paint)
        {
            ChangeToolSelected(ToolSelected.Paint);

        }
        else if(toolSelected != ToolSelected.None && !insertButton && !selectButton && !rotateButton && !deleteButton && !paintButton)
        {
            ChangeToolSelected(ToolSelected.None);
        }
    }
    public void ChangeToolSelected(ToolSelected tool)
    {
        toolSelected = tool;
        switch(toolSelected)
        {
            case ToolSelected.None:
                insertButton = false;
                selectButton = false;
                rotateButton = false;
                deleteButton = false;
                paintButton = false;

                break;
            case ToolSelected.Insert:
                insertButton = true;
                selectButton = false;
                rotateButton = false;
                deleteButton = false;
                paintButton = false;

                break;

            case ToolSelected.Select:
                insertButton = false;
                selectButton = true;
                rotateButton = false;
                deleteButton = false;
                paintButton = false;

                break;

            case ToolSelected.Rotate:
                insertButton = false;
                selectButton = false;
                rotateButton = true;
                deleteButton = false;
                paintButton = false;

                break;

            case ToolSelected.Delete:
                insertButton = false;
                selectButton = false;
                rotateButton = false;
                deleteButton = true;
                paintButton = false;

                break;

            case ToolSelected.Paint:
                insertButton = false;
                selectButton = false;
                rotateButton = false;
                deleteButton = false;
                paintButton = true;

                break;
        }
        

    }

    public void DeleteObjects()
    {
        GameObject[] objs = Selection.gameObjects;
        foreach(GameObject i in objs)
        {
            if(i.gameObject.scene.name != null)
            {
                Object.DestroyImmediate(i);
            }
        }
    }

}
public enum ToolSelected
{
    None,
    Insert,
    Select,
    Rotate,
    Delete,
    Paint
}
