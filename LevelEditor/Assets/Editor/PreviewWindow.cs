﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class PreviewWindow
{

    private PreviewRenderUtility mPrevRender;
    private List<Mesh> mPreviewMesh = new List<Mesh>();
    private List<Material> mMat = new List<Material>();
    private List<Transform> posAndRot = new List<Transform>();

    public float cameraOrbit;
    public float cameraPitch;
    public float cameraHeight;
    public float cameraZoom;

    public Texture DrawRenderPreview(Rect r)
    {
        if (mPrevRender == null)
            mPrevRender = new PreviewRenderUtility();

        if (mPreviewMesh.Count == 0 && Selection.activeGameObject != null)
        {
            MeshFilter[] meshFilters = Selection.activeGameObject.GetComponentsInChildren<MeshFilter>();
            Renderer[] renderers = Selection.activeGameObject.GetComponentsInChildren<Renderer>();

            posAndRot.AddRange(Selection.activeGameObject.GetComponentsInChildren<Transform>());
           
            foreach(MeshFilter m in meshFilters)
            {
                mPreviewMesh.Add(m.sharedMesh);
                
            }

            foreach(Renderer c in renderers)
            {
                mMat.Add(c.sharedMaterial);
            }

            
            
        }

        mPrevRender.camera.transform.position = (Vector3)(new Vector3(0, 0, -8f));
        mPrevRender.camera.transform.rotation = Quaternion.Euler(0, 0, 0);
        mPrevRender.camera.transform.RotateAround(Vector3.zero, Vector3.up, cameraOrbit);
        mPrevRender.camera.transform.RotateAround(Vector3.zero, Vector3.right, cameraPitch);
        mPrevRender.camera.fieldOfView = cameraZoom;
        mPrevRender.camera.farClipPlane = 30;

        mPrevRender.lights[0].intensity = 0.5f;
        mPrevRender.lights[0].transform.rotation = Quaternion.Euler(30f, 30f, 0f);
        mPrevRender.lights[1].intensity = 0.5f;

        mPrevRender.BeginPreview(r, GUIStyle.none);

        Vector3 posDecrease = new Vector3();

        for (int i = 0; i < mPreviewMesh.Count; i++)
        {
            if(i == 0)
            {
                posDecrease = posAndRot[i].position;
            }
            mPrevRender.DrawMesh(mPreviewMesh[i], posAndRot[i].position-posDecrease, posAndRot[i].localRotation, mMat[i], 0);
        }
        bool fog = RenderSettings.fog;
        Unsupported.SetRenderSettingsUseFogNoDirty(false);
        mPrevRender.camera.Render();
        Unsupported.SetRenderSettingsUseFogNoDirty(fog);
        Texture texture = mPrevRender.EndPreview();

        //GUI.DrawTexture(r, texture);
        mPreviewMesh.Clear();
        mMat.Clear();
        posAndRot.Clear();
        return texture;
    }
}
