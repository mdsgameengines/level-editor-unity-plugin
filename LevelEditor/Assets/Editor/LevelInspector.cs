﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelScript))]
public class LevelInspector : Editor {

    public LevelEditorBase window;
    public Transform previewGrid;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void OnInspectorGUI()
    {
        
        LevelScript script = (LevelScript)target;
        previewGrid = (Transform)EditorGUILayout.ObjectField("Preview Grid", script.previewGrid, typeof(Transform), true);
        bool open = GUILayout.Button("Open Editor");
        if (open)
        {
            LevelEditorBase.ShowWindow();
            LevelEditorBase.level = script.gameObject;
            LevelEditorBase.levelScript = script;
        }
        bool close = GUILayout.Button("Close Editor");
        if (close)
        {
            LevelEditorBase.CloseWindow();
        }
        bool closeScene = GUILayout.Button("Whatever");
        if (closeScene)
        {

            SceneView win = (SceneView)EditorWindow.GetWindow(typeof(SceneView));
           // win.Close();
        }
    }
}
