﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelScript : MonoBehaviour {

    public UnityEditor.EditorWindow window;

    public int layerInt;
    public float layerHeight;
    public int currentLayer;
    public LayerMask mask;
    public GridScript gridClass;
    public Transform previewGrid;
    
}
