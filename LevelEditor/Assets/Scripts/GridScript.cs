﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridScript : MonoBehaviour
{
    
    public GameObject level;
    public Vector2Int gridSize;
    public Vector2 nodeSize;
    public int mask;

    public Dictionary<int, GameObject> gridDict = new Dictionary<int, GameObject>();
    public Dictionary<int, float> gridHeightDict = new Dictionary<int, float>();

    Material previewMat;
    Material gridMat;


    public GridScript()
    {
        
    }

    public void SetupGrid(GameObject _level, Vector2Int _gridSize, Vector2 _nodeSize, LayerMask _mask)
    {
        level = _level;
        gridSize = _gridSize;
        nodeSize = _nodeSize;
        mask = _mask;
        
    }

    void Debugs()
    {
        Debug.Log(level.name);
    }

    public void CreateGrid(int layer, float layerHeight, bool preview)
    {
        Debugs();
        previewMat = Resources.Load<Material>("LevelEditor/PreviewMaterial") as Material;
        gridMat = Resources.Load<Material>("LevelEditor/GridMaterial") as Material;
        GameObject parent = new GameObject();
        string name = "Grid" + layer;
        parent.name = name;
        parent.transform.parent = level.transform;
        gridDict.Add(layer, parent);
        gridHeightDict.Add(layer, layerHeight);
        for (int i = 0; i < gridSize.x + 1; i++)
        {
            GameObject line = new GameObject();
            line.transform.parent = parent.transform;
            LineRenderer rend = line.AddComponent<LineRenderer>();
            rend.alignment = LineAlignment.Local;
            line.transform.rotation = Quaternion.Euler(90, 0, 0);
            line.transform.position = new Vector3(i * nodeSize.x, 0.01f, 0);
            line.layer = mask;
            rend.positionCount = 2;
            Vector3 v = line.transform.position;
            v.y = 0.01f + layerHeight;
            rend.SetPosition(0, v);
            rend.SetPosition(1, new Vector3(rend.GetPosition(0).x, 0.01f + layerHeight, rend.GetPosition(0).z + (gridSize.y * nodeSize.y)));
            rend.widthMultiplier = 0.1f;

            if (!preview)
            {
                rend.material = gridMat;
            } else
            {
                rend.material = previewMat;
            }
        }

        for (int i = 0; i < gridSize.y + 1; i++)
        {
            GameObject line = new GameObject();
            line.transform.parent = parent.transform;
            LineRenderer rend = line.AddComponent<LineRenderer>();
            rend.alignment = LineAlignment.Local;
            line.transform.rotation = Quaternion.Euler(90, 0, 0);
            line.transform.position = new Vector3(0, 0.01f, i * nodeSize.y);
            line.layer = mask;
            rend.positionCount = 2;
            Vector3 v = line.transform.position;
            v.y = 0.01f + layerHeight;
            rend.SetPosition(0, v);
            rend.SetPosition(1, new Vector3(rend.GetPosition(0).x + (gridSize.x * nodeSize.x), 0.01f + layerHeight, rend.GetPosition(0).z));
            rend.widthMultiplier = 0.1f;
            if (!preview)
            {
                rend.material = gridMat;
            }
            else
            {
                rend.material = previewMat;
            }

        }

        for (int i = 0; i < gridSize.x; i++)
        {
            for (int j = 0; j < gridSize.y; j++)
            {
                GameObject tile = GameObject.CreatePrimitive(PrimitiveType.Cube);
                tile.transform.localScale = new Vector3(nodeSize.x, 0.01f, nodeSize.y);
                tile.transform.position = new Vector3(i * nodeSize.x + (nodeSize.x / 2f), 0 + layerHeight, j * nodeSize.y + (nodeSize.y / 2f));
                tile.layer = mask;
                tile.GetComponent<Renderer>().enabled = false;
                tile.GetComponent<Collider>().isTrigger = true;
                tile.transform.parent = parent.transform;
            }
        }


    }

    public void CreatePreviewGrid(Transform t)
    {

        previewMat = Resources.Load<Material>("LevelEditor/PreviewMaterial") as Material;
        gridMat = Resources.Load<Material>("LevelEditor/GridMaterial") as Material;
        GameObject parent = new GameObject();
        string name = "PreviewGrid";
        parent.name = name;
        parent.transform.parent = t.transform;
        
        for (int i = 0; i < gridSize.x + 1; i++)
        {
            GameObject line = new GameObject();
            line.transform.parent = parent.transform;
            LineRenderer rend = line.AddComponent<LineRenderer>();
            rend.alignment = LineAlignment.Local;
            line.transform.rotation = Quaternion.Euler(90, 0, 0);
            line.transform.position = new Vector3(i * nodeSize.x, 0.01f, 0);
            line.layer = mask;
            rend.positionCount = 2;
            Vector3 v = line.transform.position;
            v.y = 0.01f;
            rend.SetPosition(0, v);
            rend.SetPosition(1, new Vector3(rend.GetPosition(0).x, 0.01f, rend.GetPosition(0).z + (gridSize.y * nodeSize.y)));
            rend.widthMultiplier = 0.1f;
            rend.material = previewMat;

        }

        for (int i = 0; i < gridSize.y + 1; i++)
        {
            GameObject line = new GameObject();
            line.transform.parent = parent.transform;
            LineRenderer rend = line.AddComponent<LineRenderer>();
            rend.alignment = LineAlignment.Local;
            line.transform.rotation = Quaternion.Euler(90, 0, 0);
            line.transform.position = new Vector3(0, 0.01f, i * nodeSize.y);
            line.layer = mask;
            rend.positionCount = 2;
            Vector3 v = line.transform.position;
            v.y = 0.01f;
            rend.SetPosition(0, v);
            rend.SetPosition(1, new Vector3(rend.GetPosition(0).x + (gridSize.x * nodeSize.x), 0.01f, rend.GetPosition(0).z));
            rend.widthMultiplier = 0.1f;rend.material = previewMat;

        }

        for (int i = 0; i < gridSize.x; i++)
        {
            for (int j = 0; j < gridSize.y; j++)
            {
                GameObject tile = GameObject.CreatePrimitive(PrimitiveType.Cube);
                tile.transform.localScale = new Vector3(nodeSize.x, 0.01f, nodeSize.y);
                tile.transform.position = new Vector3(i * nodeSize.x + (nodeSize.x / 2f), 0, j * nodeSize.y + (nodeSize.y / 2f));
                tile.layer = mask;
                tile.GetComponent<Renderer>().enabled = false;
                tile.GetComponent<Collider>().enabled = false;
                tile.transform.parent = parent.transform;
            }
        }


    }


    public void ActivateGrid(int g)
    {
        for (int i = 0; i < gridDict.Count; i++)
        {
            if (i != g)
            {
                gridDict[i].SetActive(false);
            }
            else
            {
                gridDict[i].SetActive(true);
            }
        }
    }
}
